# block_cond

Waits (blocking) for a defined condition (system load, file...), then 'releases'.

'To release' means here: to invoke a command given as a command-line argument (if any) then to exit (unblocking the shell).

For Linux (and probably any other Unix flavor offering a recent version of bash).

Use it as a flexible "batch".

## Description

BEWARE: the target "system load" value, given as a parameter (let's call it 'L') refers here to average system loads (ASL).

ASL are traditionnaly obtained thanks to the getloadavg(3) call.

This script considers any current ASL satisfying if L is superior to the 1-minute ASL, and inferior to five times the 5-minute ASL.

It can also consider blocking files.

## Sample invocations
``` shell
block_cond -l 1.5 COMMAND
```
Will block then release, invoking ''COMMAND'', as soon as L reaches 1.5.

``` shell
block_cond -f /tmp/myblock -f /tmp/otherblock -l 1.5 COMMAND
```
Will block then, if no abnormal condition was detected, invoke ''COMMAND'' as soon as L is lower than 1.5 and also (ALL conditions must be simultaneously safisfied) no file named "/tmp/myblock" nor "/tmp/otherblock" exists.

``` shell
block_cond -g GENUS -l 5 COMMAND
```
''GENUS'' is any string legal as a filename. At any moment one instance pertaining to a given GENUS blocks all other ones.  Use it to sequence.

For a given ''GENUS'':
1. only one block_cond instance obtains a lock (any other block_cond instance also invoked with the same 'GENUS' argument must wait)
2. the locking instance waits for the required context then releases XOR is interrupted.  Notice: each and any ''COMMAND''s may not be invoked, as any block_cond instance may fail (be interrupted...)

# TODO

Timeout: after a timeout, abandon (don't invoke anything, exit 4)

New criteria: atop summaries (storage, cpu, mem...). Esp.: free core memory, swap usage, disk usage as summed up by atop's 'busy'

New criteria: Postgresql caches hitratio:
index hitrate: SELECT (sum(idx_blks_hit)) / sum(idx_blks_hit + idx_blks_read) FROM pg_statio_user_indexes
cache hitrate: SELECT sum(heap_blks_hit) / (sum(heap_blks_hit) + sum(heap_blks_read)) FROM pg_statio_user_tables;
